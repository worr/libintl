# libintl

This is an attempt at modernizing and updating NetBSD's BSD-licensed libintl
implementation.

## goals

* restore gettext compat
* use modern C practices
* clean up implementations
* add test suite
* implement GNU-gettext-compatible libgettext-po
* build basic developer utilities

Once the above have been achieved

* get merged into NetBSD's source tree
* submit for inclusion into OpenBSD and FreeBSD

# TODO

## Functions

- [x] pgettext
- [x] dpgettext
- [x] dcpgettext
- [x] npgettext
- [x] dnpgettext
- [x] dcnpgettext

## Macros

- [x] pgettext\_expr
- [x] dpgettext\_expr
- [x] dcpgettext\_expr
- [x] npgettext\_expr
- [x] dnpgettext\_expr
- [x] dcnpgettext\_expr

## Utilities

- [x] gettext
- [ ] msgfmt
- [ ] msgunfmt

## libgettext-po

### Read-only APIs

- [x] po\_file API
- [x] Header entry API
- [x] po\_iterator API
- [ ] po\_message API
- [x] po\_filepos API
- [ ] Format type API
- [ ] Checking API

### Write-only APIs

- [ ] po\_file
- [ ] Header entry
- [ ] po\_iterator API
- [ ] po\_message

## Features

- [x] Message contexts
- [ ] libgettext-po
- [ ] Test suite
- [ ] GNU gettext-compatible lua library
