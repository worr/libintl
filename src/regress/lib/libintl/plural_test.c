#include <libintl.h>
#include <locale.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#define TEST(n)	dngettext("test", "1", "2", (n))

int
main(void)
{
	bindtextdomain("test", "."); /* ./LANG/LC_MESSAGES/test.mo */
	assert(strcmp("1", TEST(1)) == 0);
	assert(strcmp("2", TEST(2)) == 0);
	assert(strcmp("2", TEST(3)) == 0);
	assert(strcmp("2", TEST(4)) == 0);

	return 0;
}

