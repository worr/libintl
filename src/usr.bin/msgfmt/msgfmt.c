/*-
 * Copyright (c) 2015 William Orr <will@worrbase.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <err.h>
#include <getopt.h>
#include <locale.h>
#include <libintl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <util.h>

static char *directory;
static char *locale;
static char *output_file;
static bool vflag;

static struct option longopts[] = {
	{ "directory", required_argument, NULL, 'D' },
	{ "help", no_argument, NULL, 'h' },
	{ "locale", required_argument, NULL, 'l' },
	{ "output-file", required_argument, NULL, 'o' },
	{ "verbose", no_argument, NULL, 'v' },
	{ NULL, 0, NULL, 0 }
};

static void
usage(void)
{

	fprintf(stderr, "Usage: %s [-v] [-l LOCALE] [-d DIRECTORY] [-o OUTPUT-FILE] filename.po...\n",
	    getprogname());
}

int
main(int argc, char **argv)
{
	int ch;
	int ret = EXIT_SUCCESS;

	setlocale(LC_ALL, "");
	setprogname(argv[0]);

	while ((ch = getopt_long(argc, argv, "D:ho:v", longopts, NULL)) != -1) {
		switch (ch) {
		case 'D':
			directory = estrdup(optarg);
			break;
		case 'l':
			locale = estrdup(optarg);
			break;
		case 'o':
			output_file = estrdup(optarg);
			break;
		case 'h':
			usage();
			ret = EXIT_FAILURE;
			goto bad;
			/* NOTREACHED */
		case 'v':
			vflag = true;
			break;
		}
	}
	argc -= optind;
	argv += optind;

	if (argc == 0) {
		ret = EXIT_FAILURE;
		warnx("missing files...");
		usage();
		goto bad;
	}

bad:
	free(directory);
	free(locale);
	free(output_file);
	return ret;
}

