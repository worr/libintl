/*-
 * Copyright (c) 2015 William Orr <will@worrbase.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

%{
#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>

#include "defs.h"
#include "parse.h"
#include "gettext-po.h"
#include "gettext-po-private.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

int yydebug = 0;

struct po_file *pf = NULL;
struct po_message *cur = NULL;
struct po_message *prev = NULL;
char *current_domain = NULL;
int last_index = -1;
int max_plural = -1;
const struct po_xerror_handler *xerrhandler = NULL;

static char*
extend_buffer(char **buf, size_t *buf_len, const char *text)
{
	size_t text_strlen = 0;
	size_t buf_strlen = 0;
	char *newbuf;

	if (!text)
		return *buf;

	text_strlen = strlen(text);

	if (yydebug) {
		fprintf(stderr, "text_strlen: %zu\n", text_strlen);
		fprintf(stderr, "text: %s\n", text);
	}

	if (*buf == NULL) {
		if (! (*buf = calloc(BUFSIZ, 1))) {
			yyerror(strerror(errno));
			goto bad;
		}

		*buf_len = BUFSIZ;
	}

	buf_strlen = strlen(*buf);

	if (yydebug) {
		fprintf(stderr, "buf_strlen: %zu\n", buf_strlen);
		fprintf(stderr, "*buf_len: %zu\n", *buf_len);
	}

	while (*buf_len - (buf_strlen + 1) < text_strlen + 1) {
		if (! (newbuf = realloc(*buf, *buf_len + BUFSIZ))) {
			yyerror(strerror(errno));
			goto bad;
		}

		*buf = newbuf;
		*buf_len += BUFSIZ;
	}

	if (strlcat(*buf, text, *buf_len) >= *buf_len) {
		xerrhandler->xerror(PO_SEVERITY_FATAL_ERROR, NULL, pf->filename,
							lineno, 0, false,
		                    "truncation occured reading file");
		goto bad;
	}

	return *buf;

bad:
	free(*buf);
	return NULL;
}

static void
append_domain(const char *domain)
{
	char **cur_domain = NULL;
	size_t n_domains = 1;

	/* try iterate to last domain; exit early if there's a match */
	for (cur_domain = pf->domains; *cur_domain && strcmp(*cur_domain, domain);
	     cur_domain++, n_domains++);

	/* return if not-NULL */
	if (*cur_domain)
		return;

	if (reallocarr(&pf->domains, n_domains + 1, sizeof(*pf->domains)))
		yyerror(strerror(errno));

	/* iterate to last domain again */
	for (cur_domain = pf->domains; *cur_domain; cur_domain += 1);
	if (! (*cur_domain = strdup(domain))) {
		yyerror(strerror(errno));
		return;
	}

	*(cur_domain + 1) = NULL;
}

static void
append_msgstr_plural(const char *msgstr_plural, int index)
{

	if (index < 0)
		errx(1, "bad index");

	if (index > max_plural)
		max_plural = index;

	if (cur->msgstr_plural == NULL || cur->msgstr_plural_len < max_plural + 1) {
		cur->msgstr_plural_len = max_plural + 1;
		if (reallocarr(&cur->msgstr_plural, cur->msgstr_plural_len, sizeof(*cur->msgstr_plural))) {
			yyerror(strerror(errno));
			return;
		}
	}

	if (! (cur->msgstr_plural[index] = strdup(msgstr_plural)))
		yyerror(strerror(errno));
}

static struct po_filepos *
new_filepos(const char *raw)
{
	struct po_filepos *po_fp;
	char *colon;
	char error[256];
	int e;

	*error = '\0';
	if (! (po_fp = malloc(sizeof(*po_fp))))
		goto bad;

	if (! (colon = strchr(raw, ':')))
		goto bad;

	if (! (po_fp->filename = strndup(raw, colon - raw)))
		goto bad;

	colon++;
	if (*colon == '\0') {
		snprintf(error, sizeof(error), "Malformed location: %s", raw);
		goto bad;
	}

	po_fp->start_line = strtou(colon, NULL, 10, 1, SIZE_MAX, &e);
	if (e) {
		snprintf(error, sizeof(error), "Non-numeric line number: %s: %s",
		         colon, strerror(e));
		goto bad;
	}

	return po_fp;

bad:
	if (*error)
		yyerror(error);
	else
		yyerror(strerror(errno));

	free(po_fp);
	return NULL;
}

%}

%token MSGCTXT MSGID MSGSTR MSGID_PLURAL DOMAIN MSGSTR_PLURAL LOC
%token SOURCE_LOCATION NEWLINE
%union {
	char *str;
	int ival;
}

%type <str> baretext
%type <str> source_location

%token <ival> PLURAL_IDX;
%token <ival> OBSOLETE
%token <str> STRING
%token <str> FLAGS
%token <str> SOURCE_LOCATION

%start po

%%

po:
	| po stanza
	;

stanza:
	stanza flags
	| stanza location
	| stanza domainname
	| entry
	{
		if (! prev) {
			SLIST_INSERT_HEAD(&pf->head, cur, messages);
		} else {
			SLIST_INSERT_AFTER(prev, cur, messages);
		}

		prev = cur;
		if (! (cur = calloc(1, sizeof(*cur))))
			yyerror(strerror(errno));
	}
	;

location:
	LOC source_location NEWLINE
	;

source_location:
	source_location SOURCE_LOCATION
	{
		struct po_filepos *pfp = new_filepos($2);

		SIMPLEQ_INSERT_HEAD(&cur->head, pfp, filepositions);
	}
	| SOURCE_LOCATION
	{
		struct po_filepos *pfp = new_filepos($1);

		SIMPLEQ_INSERT_HEAD(&cur->head, pfp, filepositions);
	}

domainname:
	DOMAIN STRING NEWLINE
	{
		free(current_domain);
		if (! (current_domain = strdup($2)))
			yyerror(strerror(errno));
		append_domain($2);
	}
	;

entry:
	simple_entry
	| msgctxt simple_entry
	| plural_entry
	| msgctxt plural_entry
	| entry NEWLINE
	;

simple_entry:
	msgid msgstr
	| OBSOLETE msgid OBSOLETE msgstr
	{
		cur->is_obsolete = true;
	}
	;

plural_entry:
	msgid msgid_plural msgstr
	| OBSOLETE msgid OBSOLETE msgid_plural OBSOLETE msgstr
	{
		cur->is_obsolete = true;
	}
	;

flags:
	FLAGS
	{
		char *flag, *p, *endptr;
		int min, max, e;

		if ($1) {
			while ((flag = strsep(&$1, ","))) {
				if (!strcmp(flag, "fuzzy"))
					cur->is_fuzzy = true;
				// This picks up negated variants as well
				else if (strstr(flag, "-format")) {
					if (! (cur->format_type = strdup(flag))) {
						yyerror(strerror(errno));
					}
				} else if (strstr(flag, "range:")) {
					// advance to numeric range
					for (p = strchr(flag, ':'); *p && !isalnum(*p); p++);
					if (!p)
						yyerror("invalid range after range keyword");

					endptr = strstr(p, "..");
					if (!endptr)
						yyerror("invalid range specified: no '..'");

					min = strtou(p, NULL, 10, 0, INT_MAX, &e);
					if (e && e != ENOTSUP)
						yyerror(strerror(e));

					p = endptr + 2;
					if (! *p)
						yyerror("invalid range specified: no max value");

					max = strtou(p, NULL, 10, 0, INT_MAX, &e);
					if (e && e != ENOTSUP)
						yyerror(strerror(e));

					if (min > max)
						yyerror("invalid range specified: min > max");

					cur->range_max = max;
					cur->range_min = min;
				} else {
					xerrhandler->xerror(PO_SEVERITY_WARNING, cur,
					                    pf->filename, lineno, 0, false,
										"unknown flag. skipping");
				}
			}
			free($1);
		}
	}
	;

msgid:
	MSGID STRING NEWLINE
	{
		extend_buffer(&cur->msgid, &cur->msgid_len, $2);
		if (!cur->domain) {
			if (! (current_domain = strdup("messages")))
				yyerror(strerror(errno));
			append_domain("messages");
		}
		if (! (cur->domain = strdup(current_domain)))
			yyerror(strerror(errno));
	}
	| msgid baretext
	{
		extend_buffer(&cur->msgid, &cur->msgid_len, $2);
	}
	;

msgstr:
	MSGSTR STRING NEWLINE
	{
		extend_buffer(&cur->msgstr, &cur->msgstr_len, $2);
	}
	| msgstr baretext
	{
		extend_buffer(&cur->msgstr, &cur->msgstr_len, $2);
	}
	| msgstr_index
	| msgstr msgstr_index
	;

msgstr_index:
	MSGSTR PLURAL_IDX STRING NEWLINE
	{
		if ($2 != last_index + 1)
			yyerror("Out-of-order indices for msgid: %s\n", cur->msgid);

		if ($2 == 0) {
			extend_buffer(&cur->msgstr, &cur->msgstr_len, $3);
		}

		append_msgstr_plural($3, $2);

		last_index += 1;
	}
	| msgstr_index baretext
	{
		size_t plural_len = strlen(cur->msgstr_plural[last_index]) + 1;
		extend_buffer(&cur->msgstr_plural[last_index], &plural_len, $2);
	}

msgid_plural:
	MSGID_PLURAL STRING NEWLINE
	{
		last_index = -1;
		extend_buffer(&cur->msgid_plural, &cur->msgid_plural_len, $2);
	}
	| msgid_plural baretext
	{
		last_index = -1;
		extend_buffer(&cur->msgid_plural, &cur->msgid_plural_len, $2);
	}
	;

msgctxt:
	MSGCTXT STRING NEWLINE
	{
		if (! (cur->msgctxt = strdup($2)))
			yyerror(strerror(errno));
	}
	;

baretext:
	STRING NEWLINE
	{
		$$=$1;
	}
	;
%%

void
yyerror(const char *fmt, ...)
{
	va_list ap;
	char *fmted;

	va_start(ap, fmt);
	if (vasprintf(&fmted, fmt, ap) == -1)
		yyerror(strerror(errno));
	va_end(ap);

	xerrhandler->xerror(PO_SEVERITY_FATAL_ERROR, cur, pf->filename, lineno, 0, false, fmted);
}

#ifdef PARSE
void
xerror(int severity, po_message_t message, const char *filename,
       size_t lineno, size_t column, int multiline_p,
       const char *message_text)
{

	fprintf(stderr, "Error in %s at line %zu: %s\n", filename, lineno, message_text);
	if (severity == PO_SEVERITY_FATAL_ERROR)
		exit(1);
}


int
main(int argc, char **argv)
{
	po_message_t pm;
	po_message_iterator_t iter;
	char **domain;
	char *filename = NULL;
	const char *header;
	int min = 0, max = 0;

	if (argc == 1)
		errx(1, "Must provide filename");

	filename = argv[1];

	struct po_xerror_handler pxeh = {
		.xerror = xerror,
		.xerror2 = NULL,
	};

	pf = po_file_read(filename, &pxeh);
	iter = po_message_iterator(pf, NULL);
	header = po_file_domain_header(pf, NULL);

	printf("Header: %s\n", header);

	printf("Project-Id-Version: %s\n", po_header_field(header, "Project-Id-Version"));

	for (pm = po_next_message(iter); pm != NULL; pm = po_next_message(iter)) {
		po_message_is_range(pm, &min, &max);
		printf("msgid: %s, msgstr: %s\n", po_message_msgid(pm), po_message_msgstr(pm));
		printf("is_obsolete: %d\n", po_message_is_obsolete(pm));
		printf("is_fuzzy: %d\n", po_message_is_fuzzy(pm));
		printf("is_format: %d\n", po_message_is_format(pm, "c-format"));
		printf("is_plural: %d\n", po_message_is_plural(pm));
		printf("range_min: %d, range_max: %d\n", min, max);
		if (po_message_is_plural(pm)) {
			printf("msgstr[0]: %s\n", po_message_msgstr_plural(pm, 0));
			printf("msgstr[1]: %s\n", po_message_msgstr_plural(pm, 1));
			printf("msgstr[2]: %s\n", po_message_msgstr_plural(pm, 2));
			printf("msgstr[3]: %s\n", po_message_msgstr_plural(pm, 3));
		}
	}

	po_message_iterator_free(iter);
	po_file_free(pf);

	return 0;
}
#endif

