/*-
 * Copyright (c) 2015 William Orr <will@worrbase.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

%{
#include "defs.h"
#include "parse.h"
#include <err.h>
#include <inttypes.h>
#include <limits.h>

extern YYSTYPE yylval;

int lineno = 1;
%}

%x COMMENT
%x STR
%x FLGS
%x PLURAL

%%

\n { lineno++; return NEWLINE; }

[ \t] { /* blackhole */ }

msgctxt { return MSGCTXT; }

msgid { return MSGID; }

msgid_plural { return MSGID_PLURAL; }

msgstr { return MSGSTR; }

"\[" { BEGIN PLURAL; }

<PLURAL>[0-9]+ { yymore(); }

<PLURAL>"\]" {
	BEGIN 0;
	int err_ret = 0;
	char *loc = strchr(yytext, ']');
	*loc = '\0';

	intmax_t res = strtoi(yytext, NULL, 0, 0, INT_MAX, &err_ret);
	if (err_ret)
		errc(-err_ret, err_ret, "%s not a valid msgstr index", yytext);

	yylval.ival = res;

	return PLURAL_IDX;
}

domain { return DOMAIN; }

"\"" { BEGIN STR; }

<STR>"\\\"" { yymore(); }

<STR>"\"" {
	BEGIN 0;
	size_t len = strlen(yytext);
	yytext[len - 1] = '\0';
	yylval.str = yytext;
	return STRING;
}

<STR>. { yymore(); }

"#," { BEGIN FLGS; }

<FLGS>"\n" {
	BEGIN 0;
	lineno++;
	size_t len = strlen(yytext);
	yytext[len - 1] = '\0';
	yylval.str = yytext;
	return FLAGS;
}

<FLGS>. { yymore(); }

"#:" { return LOC; }

[a-z0-9/\.]+:[0-9]+ { yylval.str = yytext; return SOURCE_LOCATION; }

"#~" { return OBSOLETE; }

# { BEGIN COMMENT; }

<COMMENT>[^\n] { /* blackhole */ }

<COMMENT>\n { BEGIN 0; lineno++; }


%%

int
yywrap(void)
{

	return 1;
}

#ifdef SCAN
YYSTYPE yylval;

int
main(int argc, char **argv)
{
	int val;

	while ((val = yylex()) != 0)
		printf("val: %d\n yytext: %s\n lineno: %d\n yylval: %s\n\n", val, yytext, lineno, yylval.str);

	return 0;
}

#endif
