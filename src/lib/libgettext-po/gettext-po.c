/*-
 * Copyright (c) 2015 William Orr <will@worrbase.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>

#include "defs.h"
#include "gettext-po.h"
#include "gettext-po-private.h"

struct po_message *cur;
struct po_file *pf;

static void
expand_escaped(char *str)
{
	char *fp, *sp, ch, pl;

	for (fp = str, sp = str; *fp != 0;) {
		if (*fp == '\\') {
			switch (*++fp) {
			case 'a':
				*sp++ = '\a';
				fp++;
				break;
			case 'b':
				*sp++ = '\b';
				fp++;
				break;
			case 'f':
				*sp++ = '\f';
				fp++;
				break;
			case 'n':
				*sp++ = '\n';
				fp++;
				break;
			case 'r':
				*sp++ = '\r';
				fp++;
				break;
			case 't':
				*sp++ = '\t';
				fp++;
				break;
			case 'v':
				*sp++ = '\v';
				fp++;
				break;
			case '\\':
				*sp++ = '\\';
				fp++;
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
				ch = *fp++ - '0';
				pl = 0;
				while (*fp >= '0' && *fp <= '7' && pl < 2) {
					ch *= 8;
					ch += *fp++ - '0';
					pl++;
				}

				*sp++ = ch;
				break;
			default:
				*sp++ = '\\';
				break;
			}
			continue;
		}
		*sp++ = *fp++;
	}

	*sp = '\0';
}

po_message_t
po_message_create(void)
{
	struct po_message *ret;

	if (!(ret = calloc(1, sizeof(struct po_message))))
		return NULL;

	SIMPLEQ_INIT(&ret->head);

	return ret;
}

void
po_message_free(po_message_t message)
{
	struct po_message *msg = (struct po_message *)message;
	char *msgstr;
	int idx;

	free(msg->domain);
	free(msg->msgid);
	free(msg->msgid_plural);
	free(msg->msgstr);

	if (msg->msgstr_plural) {
		for (idx = 0; idx < msg->msgstr_plural_len; idx++)
			free(msg->msgstr_plural[idx]);
		free(msg->msgstr_plural);
	}


	free(msg->format_type);
	free(msg);
}

po_file_t
po_file_create(void)
{
	struct po_file *pfm;

	if (! (pfm = calloc(1, sizeof(*pfm))))
		return NULL;

	SLIST_INIT(&pfm->head);

	if (! (pfm->domains = calloc(1, sizeof(*pfm->domains)))) {
		free(pfm);
		return NULL;
	}

	return pfm;
}

po_file_t
po_file_read(const char *filename, po_xerror_handler_t pxeh)
{
	FILE *fh;
	po_message_iterator_t iter;
	po_message_t pm;
	struct po_message *p;
	char **cur_domain;
	size_t i;

	xerrhandler = pxeh;
	if (! (pf = po_file_create())) {
		pxeh->xerror(PO_SEVERITY_FATAL_ERROR, NULL, filename, 0, 0, false,
		             strerror(errno));
		return NULL;
	}

	if (! (cur = calloc(1, sizeof(*cur))))
		goto bad;

	if (! (pf->filename = strdup(filename)))
		goto bad;

	if (!(fh = fopen(filename, "r")))
		goto bad;

	yyin = fh;

	yyparse();

	for (cur_domain = pf->domains; *cur_domain; cur_domain++) {
		iter = po_message_iterator(pf, *cur_domain);
		for (pm = po_next_message(iter); pm != NULL; pm = po_next_message(iter)) {
			p = (struct po_message *)pm;
			expand_escaped(p->msgstr);
			expand_escaped(p->msgid);

			if (po_message_is_plural(pm)) {
				for (i = 0; i < p->msgstr_plural_len; i++) {
					expand_escaped(p->msgstr_plural[i]);
				}
			}
		}
	}

	return pf;

bad:
	pxeh->xerror(PO_SEVERITY_FATAL_ERROR, NULL, filename, 0, 0, false,
				 strerror(errno));

	free(pf->filename);
	free(pf);

	return NULL;
}

void
po_file_free(po_file_t file)
{
	struct po_file *f = (struct po_file *)file;
	struct po_message *item;
	char **cur_domain;

	for (cur_domain = f->domains; *cur_domain; cur_domain++)
		free(*cur_domain);
	free(f->domains);

	while (!SLIST_EMPTY(&f->head)) {
		item = SLIST_FIRST(&f->head);
		SLIST_REMOVE_HEAD(&f->head, messages);
		po_message_free(item);
	}

	free(file->filename);
}

const char * const *
po_file_domains(po_file_t file)
{
	struct po_file *f = (struct po_file *)file;

	return (const char * const *)f->domains;
}

const char *
po_file_domain_header(po_file_t file, const char *domain)
{
	struct po_file *f = (struct po_file *)file;
	po_message_t pm;
	po_message_iterator_t iter = po_message_iterator(f, domain);

	for (pm = po_next_message(iter); pm == NULL || po_message_msgid(pm)[0] == '\0'; pm = po_next_message(iter))
		if (po_message_msgid(pm)[0] == '\0')
			break;

	po_message_iterator_free(iter);

	if (pm)
		return po_message_msgstr(pm);

	return NULL;
}

char *
po_header_field(const char *header, const char *field)
{
	char *loc, *lastc;

	loc = strstr(header, field);
	if (!loc)
		return NULL;

	lastc = strchr(loc, '\n');
	if (!lastc)
		return NULL;

	loc = strchr(loc, ':');
	if (!loc)
		return NULL;

	while (*loc != '\0' && *loc != '\n' && (*loc == ':' || isspace(*loc)))
		loc++;

	return strndup(loc, lastc - loc);
}

po_message_iterator_t
po_message_iterator(po_file_t file, const char *domain)
{
	struct po_message_iterator *iter;
	struct po_file *f = (struct po_file *)file;

	if (! (iter = calloc(1, sizeof(*iter))))
		return NULL;

	iter->pf = f;
	if (domain) {
		if (! (iter->domain = strdup(domain)))
			goto bad;
	} else {
		if (! (iter->domain = strdup("messages")))
			goto bad;
	}

	return iter;

bad:
	free(iter->domain);
	free(iter);
	return NULL;
}

void
po_message_iterator_free(po_message_iterator_t iterator)
{
	struct po_message_iterator *iter = (struct po_message_iterator *)iterator;

	free(iter->domain);
	free(iter);
}

po_message_t
po_next_message(po_message_iterator_t iterator)
{
	struct po_message_iterator *iter = (struct po_message_iterator *)iterator;
	struct po_message *ret;

	if (iter->iter)
		ret = SLIST_NEXT(iter->iter, messages);
	else
		ret = SLIST_FIRST(&iter->pf->head);

	if (!ret) {
		return ret;  // End of list
	} else if (iter->domain && ret->domain && ! strcmp(iter->domain, ret->domain)) {
		iter->iter = ret;
		return ret;  // match domain
	}

	return NULL;
}

const char *
po_message_msgid(po_message_t message)
{
	struct po_message *m = (struct po_message *)message;

	return m->msgid;
}

const char *
po_message_msgid_plural(po_message_t message)
{
	struct po_message *m = (struct po_message *)message;

	return m->msgid_plural;
}

const char *
po_message_msgstr(po_message_t message)
{
	struct po_message *m = (struct po_message *)message;

	return m->msgstr;
}

const char *
po_message_msgstr_plural(po_message_t message, int index)
{
	struct po_message *m = (struct po_message *)message;
	size_t iter_idx = 0;
	char **msgstr = NULL;

	if (index >= m->msgstr_plural_len)
		return NULL;

	for (msgstr = m->msgstr_plural; m->msgstr_plural != NULL && iter_idx != index; msgstr++, iter_idx++);

	return *msgstr;
}

int
po_message_is_obsolete(po_message_t message)
{
	struct po_message *m = (struct po_message *)message;

	return m->is_obsolete;
}

int
po_message_is_fuzzy(po_message_t message)
{
	struct po_message *m = (struct po_message *)message;

	return m->is_fuzzy;
}

int
po_message_is_format(po_message_t message, const char *format_type)
{
	struct po_message *m = (struct po_message *)message;

	if (format_type == m->format_type)
		return 1;
	else if (!m->format_type)
		return 0;
	else if (!format_type)
		return 0;

	return !strcmp(format_type, m->format_type);
}

int
po_message_is_plural(po_message_t message)
{
	struct po_message *m = (struct po_message *)message;

	return m->msgstr_plural != NULL;
}

void
po_message_is_range(po_message_t message, int *minp, int *maxp)
{
	struct po_message *m = (struct po_message *)message;

	*minp = m->range_min;
	*maxp = m->range_max;
}

void
po_message_set_range(po_message_t message, int minp, int maxp)
{
	struct po_message *m = (struct po_message *)message;

	m->range_max = maxp;
	m->range_min = minp;
}

const char *
po_filepos_file(po_filepos_t filepos)
{
	struct po_filepos *fp = (struct po_filepos *)filepos;

	return fp->filename;
}

size_t
po_filepos_start_line(po_filepos_t filepos)
{
	struct po_filepos *fp = (struct po_filepos *)filepos;

	return fp->start_line;
}

