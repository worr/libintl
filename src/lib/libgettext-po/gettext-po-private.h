/*-
 * Copyright (c) 2015 William Orr <will@worrbase.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef __GETTEXT_PO_PRIVATE_H_DEFINED__
#define __GETTEXT_PO_PRIVATE_H_DEFINED__

#include <stdbool.h>
#include <sys/cdefs.h>
#include <sys/queue.h>

struct po_file {
	SLIST_HEAD(po_message_head, po_message) head;
	char **domains;
	char *filename;
};

struct po_message_iterator {
	struct po_message *iter;
	struct po_file *pf;
	char *domain;
};

struct po_message {
	bool is_fuzzy;
	bool is_obsolete;
	int range_max;
	int range_min;
	char *domain;
	char *msgctxt;
	char *msgid;
	char *msgid_plural;
	char *msgstr;
	char **msgstr_plural;
	char *format_type;
	SIMPLEQ_HEAD(po_filepos_head, po_filepos) head;
	SLIST_ENTRY(po_message) messages;
	size_t msgid_len;
	size_t msgid_plural_len;
	size_t msgstr_len;
	size_t msgstr_plural_len;
};

struct po_filepos {
	const char *filename;
	SIMPLEQ_ENTRY(po_filepos) filepositions;
	size_t start_line;
};

extern struct po_file *pf;
extern struct po_message *cur;
extern const struct po_xerror_handler *xerrhandler;

#endif /* __GETTEXT_PO_PRIVATE_H_DEFINED__ */

