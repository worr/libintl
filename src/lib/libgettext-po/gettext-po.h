/*-
 * Copyright (c) 2015 William Orr <will@worrbase.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef __GETTEXT_PO_H_DEFINED__
#define __GETTEXT_PO_H_DEFINED__

#include <sys/cdefs.h>

typedef struct po_file *po_file_t;
typedef struct po_message_iterator *po_message_iterator_t;
typedef struct po_message *po_message_t;
typedef struct po_filepos *po_filepos_t;

#define PO_SEVERITY_WARNING     0
#define PO_SEVERITY_ERROR       1
#define PO_SEVERITY_FATAL_ERROR 2
struct po_xerror_handler {
	void (*xerror) (int severity, po_message_t message, const char *filename,
	                size_t lineno, size_t column, int multiline_p,
	                const char *message_text);

	void (*xerror2) (int severity, po_message_t message1,
	                 const char *filename1, size_t lineno1, size_t column1,
	                 int multiline_p1, const char *message_text1,
	                 po_message_t message2, const char *filename2,
	                 size_t lineno2, size_t column2, int multiline_p2,
	                 const char *message_text2);
};
typedef const struct po_xerror_handler *po_xerror_handler_t;

__BEGIN_DECLS
/* po_file_t */

/* Creates an empty po file in memory */
po_file_t po_file_create(void);

/* Reads a po from a file into memory */
po_file_t po_file_read(const char *, po_xerror_handler_t);

/* Frees an in-memory po */
void po_file_free(po_file_t);

/* Returns a NULL-terminated list of domains in a po */
const char * const *po_file_domains(po_file_t);

/* header API */

/* Returns domain header in po file or NULL if not found */
const char *po_file_domain_header(po_file_t, const char *);
char *po_header_field(const char *, const char *);

/* po_message_iterator_t */

/* Creates a new iterator over messages in a po file for a given domain */
po_message_iterator_t po_message_iterator(po_file_t, const char *);

/* Frees an iterator returned by po_message_iterator */
void po_message_iterator_free(po_message_iterator_t);

/* Gets the next po_message_t from a po_message_iterator_t */
po_message_t po_next_message(po_message_iterator_t);

/* po_message_t */

po_message_t po_message_create(void);

/* Not provided by libgettext-po, but provided for symmetry
 * with po_message_create
 */
void po_message_free(po_message_t);

/* po_message_t accessors */

const char *po_message_msgid(po_message_t);
const char *po_message_msgid_plural(po_message_t);
const char *po_message_msgstr(po_message_t);
const char *po_message_msgstr_plural(po_message_t, int);
int po_message_is_obsolete(po_message_t);
int po_message_is_fuzzy(po_message_t);
int po_message_is_plural(po_message_t);
int po_message_is_format(po_message_t, const char *);
void po_message_is_range(po_message_t, int *, int *);

/* po_message_t mutators */
void po_message_set_range(po_message_t, int, int);

/* po_filepos_t */
const char *po_filepos_file(po_filepos_t);
size_t po_filepos_start_line(po_filepos_t);
__END_DECLS

#endif /* __GETTEXT_PO_H_DEFINED__ */

